package id.ac.ui.cs.advprog.tutorial0.model;

import java.util.stream.Stream;

public enum Day {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY;



    public static Stream<Day> stream() {
        return Stream.of(Day.values());
    }
}


